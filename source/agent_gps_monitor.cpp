/********************************************************************
* Copyright (C) by Hawaii Space Flight Laboratory (HSFL)
* <https://www.hsfl.hawaii.edu/>
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.org>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

// COSMOS libraries
#include <core/libraries/support/configCosmos.h>
#include <core/libraries/agent/agentclass.h>

// C++ libraries
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{
    cout << "Monitor GPS" << endl;

    Agent *agent;
    string nodename = "cubesat1";
    string agentname = "gps_monitor";

    agent = new Agent(nodename, agentname);

    int32_t iretn;
    Agent::messstruc mess;

    // Start executing the agent
    while(agent->running())
    {

        iretn = agent->readring(mess, Agent::AgentMessage::BEAT, 5., (Agent::Where)1);

        //        cout << mess.meta.beat.proc << endl;

        if (iretn > 0 && !strcmp(mess.meta.beat.proc, "gps"))
        {
            json_parse(mess.adata, agent->cinfo);

            double gps_latitude = agent->cinfo->devspec.gps[0]->geods.lat;
            double gps_longitute = agent->cinfo->devspec.gps[0]->geods.lon;
            double gps_altitude = agent->cinfo->devspec.gps[0]->geods.h;

            cout << "GPS (lat, lon, h) = [" << gps_latitude << ", " <<  gps_longitute << ", " << gps_altitude << "]" << endl;

        }
        //sleep for 1 sec
        COSMOS_SLEEP(1.0);
    }

    return 0;
}

