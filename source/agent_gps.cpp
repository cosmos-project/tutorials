/********************************************************************
* Copyright (C) by Hawaii Space Flight Laboratory (HSFL)
* <https://www.hsfl.hawaii.edu/>
*
* This file is part of the COSMOS/core that is the central
* module for COSMOS. For more information on COSMOS go to
* <http://cosmos-project.org>
*
* The COSMOS/core software is licenced under the
* GNU Lesser General Public License (LGPL) version 3 licence.
*
* You should have received a copy of the
* GNU Lesser General Public License
* If not, go to <http://www.gnu.org/licenses/>
*
* COSMOS/core is free software: you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation, either version 3 of
* the License, or (at your option) any later version.
*
* COSMOS/core is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* Refer to the "licences" folder for further information on the
* condititons and terms to use this software.
********************************************************************/

// COSMOS libraries
#include <core/libraries/support/configCosmos.h>
#include <core/libraries/agent/agentclass.h>
#include <core/libraries/device/serial/serialclass.h>
#include <core/libraries/support/stringlib.h>
#include "nmea.h"

// C++ libraries
#include <iostream>
#include <fstream>

using namespace std;

devicestruc* device_from_name(string device_name, cosmosstruc* cinfo);

int main(int argc, char** argv)
{
    cout << "Agent GPS" << endl;

    // ------------------------------------------
    // Setup agent
    Agent *agent;
    string nodename = "cubesat1";
    string agentname = "gps";

    agent = new Agent(nodename, agentname);


    // ------------------------------------------
    // Setup Serial Port

    // if there is no arguments use default serial port
    string serial_port = "/dev/ttyUSB0";
    size_t serial_baud = 9600;

    // set up reading from serial port
    Serial serial_gps = Serial(serial_port,serial_baud);
    serial_gps.set_timeout(0, 3.);

    cout << "serial port: " << serial_port << " at " << serial_baud << endl;
    if (serial_gps.get_error() < 0) {
        // there was error opening the serial port, close the program
        cout << "error opening serial port: " << serial_gps.get_error()  << endl;
        return 0;
    } else {
        cout << "sucess opening serial port " << endl;
    }

    // ------------------------------------------
    // Setup State of Health Messages

    // add state of health (soh)
    //    string soh = "{\"device_batt_current_000\"}";

    // SOH for GPS location information: device_gps_geods (device GPS with geodetic position (s) information)
    string soh = "{\"node_loc_utc\","
                 "\"device_gps_geods_000\"}" ;

    // TODO: implement something like
    //soh.add.gps(0)

    // set the soh string
    // TODO: Add verifcation that device_gps_geods exists
    // TODO: re-evaluate definition of SOH
    agent->set_sohstring(soh.c_str());

    // Start executing the agent
    while(agent->running())
    {

        // collect GPS information

        int32_t status;
        string gpsdata;

        // read serial port from the arduino
        // reads the first line and saves it in jsonstring


        //        // option 1
        status = serial_gps.get_string(gpsdata, '\n');
        //        cout << gpsdata;

        // option 2
        // TODO: check with Eric
        //        status = serial_gps.get_nmea(gpsdata,80);
        //        cout << gpsdata;

        StringParser nmea(gpsdata);
        string nmea_id = nmea.getFieldNumber(1);

        //        cout << nmea_id << endl;

        double gps_latitude = 0;
        double gps_longitude = 0;
        double gps_altitute = 0;

        if (nmea_id.compare("$GPGGA") == 0) {

            string time_utc = nmea.getFieldNumber(2);

            int hours = stoi(time_utc.substr(0,2));
            int minutes = stoi(time_utc.substr(2,2));
            float seconds = stof(time_utc.substr(4,6));


            string lat_str = nmea.getFieldNumber(3);
            string indicator_ns = nmea.getFieldNumber(4);
            string lon_str  = nmea.getFieldNumber(5);
            string indicator_ew = nmea.getFieldNumber(6);
            int gps_quality = nmea.getFieldNumberAsInteger(7);
            int num_sats = nmea.getFieldNumberAsInteger(8);
            gps_altitute = nmea.getFieldNumberAsDouble(10);

            double lat_dd = stod(lat_str.substr (0,2));
            double lat_mm = stod(lat_str.substr (2,8));

            double lon_dd = stod(lon_str.substr (0,3));
            double lon_mm = stod(lon_str.substr (3,9));

            // convert to decimal degrees
            gps_latitude = lat_dd + lat_mm/60.;
            gps_longitude = lon_dd + lon_mm/60.;

            cout << gpsdata;
            //            cout << "Time UTC:\t" << time_utc << endl;
            cout << "Time UTC:\t" << hours << ":" << minutes << ":" << fixed << setprecision(3) << seconds << endl;
            //            cout << "Lat:\t" << lat << endl;
            cout << "Lat:\t" << lat_dd << "d " << lat_mm << "' " << indicator_ns << endl;
            cout << "Lat:\t" << setprecision (10) << gps_latitude << endl;
            //            cout << "N/S:\t" << indicator_ns << endl;
            //            cout << "Lon:\t" << lon << endl;
            cout << "Lon:\t" << lon_dd << "d " << lon_mm << "' " << indicator_ew << endl;
            //            cout << "E/S:\t" << indicator_ew << endl;
            cout << "Lon:\t" << setprecision (10) << gps_longitude << endl;
            cout << "Quality:\t" << gps_quality << endl;
            cout << "# Sats:\t" << num_sats << endl;
            cout << "Alt:\t" << gps_altitute << endl;


            // get the GPS device object address
            devicestruc* device_gps = device_from_name("GPS",agent->cinfo);

            // check the device gps
            if (device_gps != nullptr) {

                //        // set the GPS information to the COSMOS namespace corresponding to the GPS
                agent->cinfo->devspec.gps[0]->utc = currentmjd(0); // TODO: change to current_time
                agent->cinfo->devspec.gps[0]->geods.lat = gps_latitude; // TODO: change devspec to device, change device to component?
                agent->cinfo->devspec.gps[0]->geods.lon = gps_longitude;
                agent->cinfo->devspec.gps[0]->geods.h = gps_altitute;


                //            agent->post(Agent::AgentMessage::SOH, json_of_table(myjstring, logtable, agent->cinfo));
            }


        }



        // TODO: log data using agent exec

        //sleep for 0.1 sec
        COSMOS_SLEEP(0.1);
    }

    return 0;
}


devicestruc* device_from_name(string device_name, cosmosstruc* cinfo) {

    devicestruc* device = nullptr;
    for (size_t i=0; i< cinfo->device.size(); ++i) {
        char* device_name_from_list = cinfo->pieces[cinfo->device[i].all.pidx].name;
        if (device_name == device_name_from_list) {
            device = &cinfo->device[i];
            return device;
        }
    }

    return device;
}
