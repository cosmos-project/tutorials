# COSMOS Tutorials Repository #

Collection of source code for the COSMOS tutorials [COSMOS Tutorials](https://docs.google.com/document/d/1nrOId7-6iBiTzjoFnFobEZpwlmBhdKSEu3mWkHiQ094/) 

Quick Start:

1. Clone this repository to cosmos/source/projects/tutorials

```
cd cosmos/source/projects
git clone git@bitbucket.org:cosmos-project/tutorials.git
```

2. Open the file CMakeLists.txt in Qt Creator 

3. Hit "Build"
 
4. "Run" one of the tutorials
